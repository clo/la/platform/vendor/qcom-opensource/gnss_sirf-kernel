# Makefile for use with Android's kernel/build system
KBUILD_OPTIONS += GNSS_SIRF_KERNEL_ROOT=$(shell pwd)
KBUILD_OPTIONS += KERNEL_ROOT=$(ROOT_DIR)/$(KERNEL_DIR)
KBUILD_OPTIONS += MODNAME=gnss_sirf
GNSS_SIRF_BLD_DIR := $(TOP)/vendor/qcom/opensource/gnss_sirf-kernel/

all: modules

modules dtbs:
	$(MAKE) -C $(KERNEL_SRC) M=$(M) modules $(KBUILD_OPTIONS)

modules_install:
	$(MAKE) M=$(M) -C $(KERNEL_SRC) modules_install

clean:
	$(MAKE) -C $(KERNEL_SRC) M=$(M) clean
