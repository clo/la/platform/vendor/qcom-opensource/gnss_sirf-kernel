LOCAL_PATH := $(call my-dir)
DLKM_DIR := $(TOP)/device/qcom/common/dlkm

#KBUILD_OPTIONS
KBUILD_OPTIONS += KERNEL_ROOT=$(TOP)/kernel/msm-$(TARGET_KERNEL_VERSION)/
KBUILD_OPTIONS += MODNAME=gnss_sirf
KBUILD_OPTIONS += BOARD_PLATFORM=$(TARGET_BOARD_PLATFORM)
KBUILD_OPTIONS += CONFIG_GNSS_SIRF=y
$(info value of TARGET_USES_KERNEL_PLATFORM IS '$(TARGET_USES_KERNEL_PLATFORM)')

#Clear Environment Variables
include $(CLEAR_VARS)
#Defining the local options
#$(shell find $(LOCAL_PATH)/gnss_sirf/ -L -type f)
LOCAL_SRC_FILES		:=  \
			$(LOCAL_PATH)/gnss_sirf/gnss_sirf.h \
			$(LOCAL_PATH)/gnss_sirf/gnss_sirf.c \
			$(LOCAL_PATH)/Android.mk \
			$(LOCAL_PATH)/Kbuild \
			$(LOCAL_PATH)/gnss_sirf-board.mk \
			$(LOCAL_PATH)/gnss_sirf-product.mk

LOCAL_MODULE_PATH	:= $(KERNEL_MODULES_OUT)
LOCAL_MODULE		:= gnss_sirf.ko
LOCAL_MODULE_TAGS	:= optional


include $(DLKM_DIR)/Build_external_kernelmodule.mk
